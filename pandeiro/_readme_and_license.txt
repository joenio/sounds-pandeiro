Sound pack downloaded from Freesound
----------------------------------------

"Tambourine"

This pack of sounds contains sounds by the following user:
 - Paulo_Gonçalves ( https://freesound.org/people/Paulo_Gon%C3%A7alves/ )

You can find this pack online at: https://freesound.org/people/Paulo_Gon%C3%A7alves/packs/11555/


Licenses in this pack (see below for individual sound licenses)
---------------------------------------------------------------

Attribution 4.0: https://creativecommons.org/licenses/by/4.0/


Sounds in this pack
-------------------

  * 182570__pgonsilva__pandeiro-plat-03.wav
    * url: https://freesound.org/s/182570/
    * license: Attribution 4.0
  * 182569__pgonsilva__pandeiro-trill.wav
    * url: https://freesound.org/s/182569/
    * license: Attribution 4.0
  * 182567__pgonsilva__pandeiro-low.wav
    * url: https://freesound.org/s/182567/
    * license: Attribution 4.0
  * 182566__pgonsilva__pandeiro-plat-01.wav
    * url: https://freesound.org/s/182566/
    * license: Attribution 4.0
  * 182565__pgonsilva__pandeiro-plat-02.wav
    * url: https://freesound.org/s/182565/
    * license: Attribution 4.0


